// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false

let store = {
    debug: true,
    authError: '',
    recoveryTokenStatus: false,
    recoverMsg : '',
    state: {
        authToken: ''
    },
    setAuthToken: function (newValue) {
        if (this.debug)
            console.log('set_auth_token = ', newValue)
        this.state.authToken = newValue;
    },
    setError: function (newValue) {
        if (this.debug)
            console.log('error = ', newValue)
        this.authError = newValue;
    },
    clearError: function () {
        this.authError = '';
    },
    setRecoveryTokenStatus: function (newValue) {
        if (this.debug)
            console.log('recoveryTokenStatus = ', newValue);
        this.recoveryTokenStatus = newValue;
    },
    setRecoveryMsg: function (newValue) {
        if (this.debug)
            console.log('recoverMsg = ', newValue);
        this.recoverMsg = newValue;
    }
}

let aXinstance = axios.create({
    baseURL: "http://localhost:8080/api/v1/",
});

let carvan = {
    //Auth user
    auth: function (login, password) {
        return aXinstance.request({
            method: 'post',
            url: 'user/sign-in',
            data: {"email": login, "password": password}
        })
            .then((response) => {
                store.clearError();
                store.setAuthToken(response.data.token);
            })
            .catch((error) => {
                store.setError(error.response.data.error);
            });
    },
    forgotpass: function (email) {
        return aXinstance.request({
            method: 'post',
            url: 'user/recovery',
            data: {"email": email}
        }).then((response) => {
            store.clearError();
        }).catch((error) => {
            store.setError(error.response.data.error)
        })
    },
    checkRecoveryHash: function (hash) {
        return aXinstance.request({
            method: 'post',
            url: 'user/recovery/'+ hash +'/check',
        }).then((response) => {
            store.setRecoveryTokenStatus(true);
        }).catch((error) => {
            setRecoveryTokenStatus(false);
        })
    },
    setNewPassword: function (password, repassword, hash) {
        return aXinstance.request({
            method: 'post',
            url: 'user/recovery/' + hash,
            data: {"password" : password, "repeat" : repassword}
        }).then((response) => {
            store.clearError()
            store.setRecoveryMsg(response.data.token)
        }).catch((error) => {
            store.setError(error.response.data.message)
        })
    }
}
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    data: {
        store: store,
        aXinstance: aXinstance,
        carvan: carvan
    },
    components: {App},
    template: '<App/>'
})
