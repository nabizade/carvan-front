import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import ForgotPass from '@/components/ForgotPass'
import Recovery from '@/components/Recovery'

Vue.use(Router)

export default new Router({
    routes: [
        {path: '/', name: 'Home', component: Home},
        {path: '/login', name: 'Login', component: Login},
        {path: '/lostpassword', name: 'ForgotPass', component: ForgotPass },
        {path: '/dashboard', name: 'Dashboard', component: Dashboard},
        {path: '/recovery/:hash/form', name: Recovery, component: Recovery}
    ]
})
